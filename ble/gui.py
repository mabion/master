
from os import stat
import parameters
import tkinter as tk
from tkinter import StringVar, filedialog, messagebox

from collections import defaultdict
import os
import dcm_sub_func
import inter_func
import kon_sub_func


class ChecklistBox(tk.Frame):
    def __init__(self, parent, kondict, **kwargs):
        tk.Frame.__init__(self, parent, **kwargs)
        self.mapset = StringVar()
        self.vars = []
        self.kondict = kondict

        self.scrollbar = tk.Scrollbar(self)
        self.scrollbar.pack(side=tk.RIGHT, fill="y")

        self.checklist = tk.Text(self, yscrollcommand=self.scrollbar.set)
        self.checklist.pack(side=tk.LEFT, expand=1, anchor="nw", fill='y')

        if isinstance(kondict, list):
            self.showlist()
        elif isinstance(kondict, dict):
            self.showdict()

        self.scrollbar.config(command = self.checklist.yview)

    def ToggleSelection(self, state):
        for child in self.checklist.winfo_children():
            child["state"] = state

    def getCheckedItems(self):
        values = []
        for var in self.vars:
            value = var.get()
            if value:
                values.append(value)
        return values

    def getKonItems(self):
        return self.mapset.get()

    def showlist(self):
        lb = tk.Label(self, text="Select Mapset: ", anchor='w', padx=10)
        self.checklist.window_create(tk.END, window=lb)
        self.checklist.insert(tk.END, "\n")
        for file in self.kondict:
            rb = tk.Radiobutton(self.checklist, text=file, value=file, variable=self.mapset , anchor="w", relief="flat", background="white", highlightthickness=0, padx=30)
            self.checklist.window_create(tk.END, window=rb)
            self.checklist.insert(tk.END, "\n")

    def showdict(self):
        for dir, konfiles in self.kondict.items():
            lb = tk.Label(self, text=dir+": ", anchor='w', padx=10)
            self.checklist.window_create(tk.END, window=lb)
            self.checklist.insert(tk.END, "\n")
            for konfile in konfiles:
                var = tk.StringVar()
                self.vars.append(var)
                cb = tk.Checkbutton(self.checklist, var=var, text=konfile, onvalue=dir+"/"+konfile, offvalue="", anchor="w", relief="flat", background="white", highlightthickness=0, padx=30)
                self.checklist.window_create(tk.END, window=cb)
                self.checklist.insert(tk.END, "\n")

class popupWindow(object):

    def __init__(self, master, InDcmDir, Vob):
        
        top=self.top=tk.Toplevel(master)
        top.title('Pop Up')
        top.geometry("1100x500")

        self.CheckVar = tk.StringVar()
        self.value_check = tk.IntVar()
        self.Dcm_dict = {}

        self.DcmFiles = parameters.ListFile(InDcmDir, (".dcm", ".DCM", ".Dcm"))
        # self.KonFiles = parameters.ListFile(InKonDir, (".kon", ".KON", ".Kon"))
        self.KonDict = kon_sub_func.kon_pre_loader(Vob)

        self.b_ExtractDcm = tk.Button(top, text="Extract DCM", command=self.ExtractDcm, height=2, width=15)
        self.b_Tunning = tk.Button(top, text="Tunning", command=self.Tunning, height=2, width=15)
        self.b_isMapset = tk.Checkbutton(top, variable=self.value_check, text='Disable Mapset Tunning', command=self.disable_enable_button)

        tk.Label(top, text='DCM Selection:').place(x=10, y=10)
        tk.Label(top, text='KON Selection:').place(x=510, y=10)

        self.CreateSelDcmList()
        self.CreateSelKonList()
        
    def disable_enable_button(self):
        if self.value_check.get():
            self.Mapset.ToggleSelection("disabled")
        else:
            self.Mapset.ToggleSelection("normal")

    def Tunning(self):
        mapset = self.Mapset.getKonItems()
        konfiles = self.checklist.getCheckedItems()

        if len(konfiles) >= 0:
            pass
            if self.value_check.get() and mapset:
                inter_func.tun_dict_sorter(self.Dcm_dict, mapset, konfiles)
            inter_func.other_dict_sorter(self.Dcm_dict, mapset, konfiles)
        else:
            messagebox.showerror("Error", "Mapset, Kon >> An option must be selected")
            self.cleanup()

    def ExtractDcm(self):
        value = self.CheckVar.get()
        if value:
            for dcm in self.DcmFiles:
                if value in dcm:
                    dcm_tun_list, dcm_other_list, self.Dcm_dict = dcm_sub_func.dcm_var_coded_sorter( dcm_sub_func.dcm_var_sorter( filter(None, dcm_sub_func.dcm_loader(dcm) )))
                    self.Mapset = ChecklistBox(self.top, list(self.Dcm_dict.keys()), bd=1, relief="sunken")
                    self.Mapset.place(x=30, y=220, width=250, height=200)
                    self.b_isMapset.place(x=300, y=220)
        else:
            messagebox.showerror("Error", "An option must be selected")
            self.cleanup()

    def CreateSelDcmList(self):

        if len(self.DcmFiles) < 1:
            messagebox.showerror("Error", "DCM: File not found")
            self.cleanup()

        y = 40
        for dcm in self.DcmFiles:
            rb = tk.Radiobutton(self.top, text=os.path.basename(dcm), value=os.path.basename(dcm), variable=self.CheckVar)
            rb.place(x=20, y=y)
            y += 30

        self.b_ExtractDcm.place(x=10, y=y)
        self.b_Tunning.place(x=10, y=y+50)

    def CreateSelKonList(self):
        if len(self.KonFiles) < 1:
            messagebox.showerror("Error", "KON: File not found")
            self.cleanup()

        self.rbDict_Kon = defaultdict(list)
        Dict_Kon = defaultdict(list)
        for kon in self.KonFiles:
            koninfo = list(filter(None, (kon.replace("\\","/")).split('/')))
            Dict_Kon[str(koninfo[-2])].append(str(koninfo[-1]))

        self.checklist = ChecklistBox(self.top, Dict_Kon, bd=1, relief="sunken")
        self.checklist.place(x=510, y=40, width=500, height=380)

    def cleanup(self):
        self.top.destroy()

class mainWindow(object):

    def __init__(self,master):
        self.master=master
        self.b_ExtractDcm = tk.Button(master, text="OK", command=self.popup, height=2, width=8)
        self.b_ExtractDcm.place(x=400, y=100)

        tk.Label(master, text="Dcm Path").place(x=10 , y=25)
        tk.Label(master, text="Kon Path").place(x=10 , y=55)
        tk.Button(master,text="...",command=self.AskDirDcm).place(x=425, y=20)
        tk.Button(master,text="...",command=self.AskDirKon).place(x=425, y=50)
      
        self.e_DcmDir = tk.Entry(master, width=55)
        self.e_DcmDir.place(x=75 , y=25)

        self.e_KonDir = tk.Entry(master, width=55)
        self.e_KonDir.place(x=75 , y=55)

    def popup(self):
        if self.e_DcmDir.get()== "" or self.e_KonDir.get() == "":
            messagebox.showerror("Error", "Dcm or Kon is empty")
        elif not os.path.isdir(self.e_KonDir.get()) or not os.path.isdir(self.e_KonDir.get()):
            messagebox.showerror("Error", "Dcm or Kon folder not found")
        else:
            self.w=popupWindow(self.master, self.e_DcmDir.get(), self.e_KonDir.get())
            self.b_ExtractDcm["state"] = "disabled" 
            self.master.wait_window(self.w.top)
            self.b_ExtractDcm["state"] = "normal"

    def AskDirDcm(self):
        self.DcmDir = filedialog.askdirectory()
        if self.DcmDir:
            self.e_DcmDir.delete(0, tk.END)
            self.e_DcmDir.insert(0, self.DcmDir)

    def AskDirKon(self):
        self.KonDir = filedialog.askdirectory()
        if self.KonDir:
            self.e_KonDir.delete(0, tk.END)
            self.e_KonDir.insert(0, self.KonDir)



# if __name__ == "__main__":
def popup():
    root=tk.Tk()
    root.title('Hello Python')
    root.geometry("500x200+10+10")


    m=mainWindow(root)
    root.mainloop()


