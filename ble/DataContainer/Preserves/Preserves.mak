# $ZFLSHEADER$ ----------------------------------------------------------------
# -----------------------------------------------------------------------------  
#                                                                            
#  Copyright (c) ZF Lenksysteme GmbH, Germany                                
#                All rights reserved                                         
#                                                                            
# ----------------------------------------------------------------------------- 
#                                                                            
#   Dateiname:       Preserves.mak
# 
#   Zustaendigkeit:  EZPx 
# 
#   Beschreibung:    Preserves makefile (project dependencies)
#                                                                                                    
# -----------------------------------------------------------------------------                                                                        

FEAT = FEAT_DEV_PRESERVES_VAR1

$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\ActiveDamping.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\ActiveInertia.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\ActiveReturn.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\ApplicationStateMachine.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\AssistanceV3.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\AutomaticParkingControl.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\BlockedSteeringDetection.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\BoostCurveInterpolation.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\CenterFeelTorque.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\DriverTorque.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\EndStop.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\EndstopHSD.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\FrictionCompensation.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\AdasMode.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\GroundHandler.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\HysteresisTorque.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\InertiaModification.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\MA.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\MotorTorqueLimiter.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\OnCenterConnection.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\RackForce.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\RackPos.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\RackPosCorr.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\RackPositionControl.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\RackPosSync.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\SteeringControlV2.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\SteeringFeelCoordinator.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\SteerTorqueControllerPlus.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\SteeringVibration.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\SupplyVoltageReduction.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\VehicleSpeedSensorV2.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\WaterHandler.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\TorqueSensorFallback.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\TorqueSignalConditioning.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\RackProtection.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\SafetyRampController.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\StateManagement.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\SafetyController.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\SafetyControllerInterface.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_01\BatVoltMon_OemWarning.kon

FEAT = FEAT_DEV_PRESERVES_VAR2

$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\ActiveDamping.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\ActiveInertia.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\ActiveReturn.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\ApplicationStateMachine.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\AssistanceV3.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\BoostCurveInterpolation.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\CenterFeelTorque.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\DriverTorque.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\EndStop.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\EndstopHSD.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\FrictionCompensation.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\GroundHandler.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\HysteresisTorque.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\InertiaModification.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\MA.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\MotorTorqueLimiter.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\OnCenterConnection.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\RackForce.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\RackPos.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\RackPosCorr.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\RackPositionControl.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\RackPosSync.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\SteeringControlV2.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\SteeringFeelCoordinator.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\SteerTorqueControllerPlus.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\SupplyVoltageReduction.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\VehicleSpeedSensorV2.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\WaterHandler.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\TorqueSensorFallback.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\TorqueSignalConditioning.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\RackProtection.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\SafetyRampController.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\StateManagement.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\SafetyController.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\SafetyControllerInterface.kon
$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Preserves\Preserves_var_02\BatVoltMon_OemWarning.kon


# $END_ZFLSHEADER$ ------------------------------------------------------------ 





# **************************************************************************************************
# HISTORY:
# --------
# 
# --------------------------------------------------------------------------------------------------
# Unter anderem: Endekennung fuer neuen KM-Info-Trigger eingef�gt
#  2009-12-09 Berckhemer Uwe SGD EZPA * (g52672)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\1
# --------------------------------------------------------------------------------------------------
#  2010-01-22 Sutter Michael SGD EZPQ * (g51902)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\2
# --------------------------------------------------------------------------------------------------
#  2010-02-25 --- (gx0837)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\3
# --------------------------------------------------------------------------------------------------
#  2010-04-09 Mergenthaler Thomas SGD EZPA * (g52484)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\4
# --------------------------------------------------------------------------------------------------
#  2010-05-21 Klotz Dietmar SGD EXT (gx0838)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\5
# --------------------------------------------------------------------------------------------------
#  2011-08-17 Tadenfok Nguetse Gilbert SGD EZPB * (g52836)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\int_wp_job35270\1
# --------------------------------------------------------------------------------------------------
#  2011-08-25 Klotz Dietmar SGD EXT (gx0838)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\6
# --------------------------------------------------------------------------------------------------
#  2011-09-09 Beer Florian SGD EXT (gx1194)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\int_wp_job35937\1
# --------------------------------------------------------------------------------------------------
#  2011-09-13 Klotz Dietmar SGD EXT (gx0838)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\7
# --------------------------------------------------------------------------------------------------
#  2012-05-31 Haubelt Florian SGD ZEEPF * (g52818)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\int_wp_job28930\1
# --------------------------------------------------------------------------------------------------
#  2012-09-03 Rieker Michael SGD ZEEPA (g53199)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\int_wp_job28930\2
# --------------------------------------------------------------------------------------------------
#  2012-09-04 Rieker Michael SGD ZEEPA (g53199)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\9
# --------------------------------------------------------------------------------------------------
#  2012-09-21 Waldenmaier Florian SGD ZEEPI * (g52893)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\int_wp_job43517\1
# --------------------------------------------------------------------------------------------------
#  2012-11-12 Rieker Michael SGD ZEEPA (g53199)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\10
# --------------------------------------------------------------------------------------------------
#  2013-02-28 Reuter Stephan SGD ZEEPF * (g53168)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\int_wp_job49903\1
# --------------------------------------------------------------------------------------------------
#  2013-03-07 --- (gx1487)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\11
# --------------------------------------------------------------------------------------------------
#  2013-05-07 Beigelbeck Daniel SGD PC-DESF * (g52710)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\int_wp_job55802\1
# --------------------------------------------------------------------------------------------------
#  2013-05-21 --- (gx1487)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\12
# --------------------------------------------------------------------------------------------------
#  2013-06-06 --- (G51902)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\int_wp_job53628\1
# --------------------------------------------------------------------------------------------------
#  2013-06-25 --- (gx1487)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\13
# --------------------------------------------------------------------------------------------------
#  2013-07-03 --- (gx1487)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\14
# --------------------------------------------------------------------------------------------------
#  2013-09-02 Wacker Andreas SGD PC-DESA * (g52614)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\int_wp_job53800\1
# --------------------------------------------------------------------------------------------------
#  2013-09-26 --- (gx1487)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\15
# --------------------------------------------------------------------------------------------------
#  2014-03-27 --- (g53523)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\int_var_sdk22\int_wp_job68632\1
# --------------------------------------------------------------------------------------------------
#  2014-03-28 --- (gx1478)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\int_var_sdk22\1
# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
#  2014-09-15 --- (gx1478)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\int_var_sdk22\int_wp_job61512\3
# --------------------------------------------------------------------------------------------------
#  2014-11-26 --- (gx1478)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\int_var_sdk22\3
# --------------------------------------------------------------------------------------------------
#  2015-01-21 --- (gx1478)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\16
# --------------------------------------------------------------------------------------------------
#  2015-07-08 Giessmann Rouven-Alexander SGD PC-DESA (g53473)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\int_var_sdk234\int_wp_95185\2
# --------------------------------------------------------------------------------------------------
#  2015-07-08 Halavart Cafer SGD EXT (gx1487)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\int_var_sdk234\1
# --------------------------------------------------------------------------------------------------
#  2015-09-25 Sutter Michael SGD PC-DESA * (g51902)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\int_var_sdk234\2
# --------------------------------------------------------------------------------------------------
#  2015-10-09 Halavart Cafer SGD EXT (gx1487)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\17
# --------------------------------------------------------------------------------------------------
#  2016-01-20 Erdmann Dominik SGD PC-DSSA * (g52584)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\int_wp_job98023\1
# --------------------------------------------------------------------------------------------------
#  2016-03-07 Schaal Thomas SGD EXT (gx1478)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\18
# --------------------------------------------------------------------------------------------------
#  2016-08-08 Vaas Rainer SGD AS-ER/ENK2 (g52975)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\int_wp_job123965\1
# --------------------------------------------------------------------------------------------------
#  2016-12-15 Pavel Stefan SGD AS-ER/ENM1 * (g51949)
#             \sgdcc_eps_sdk3\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\3
# --------------------------------------------------------------------------------------------------
#  2016-12-19 Gross Marcel SGD AS-ER/ENM2 * (g52690)
#             \sgdcc_eps_sdk3\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\4
# --------------------------------------------------------------------------------------------------
#  2016-12-21 Gross Marcel SGD AS-ER/ENM2 * (g52690)
#             \sgdcc_eps_sdk3\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\5
# --------------------------------------------------------------------------------------------------
#  2017-03-15 Mozic Danijel SGD AS-ER/ENN1 * (g40716)
#             \sgdcc_eps_vcc_maintrack\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\3
# --------------------------------------------------------------------------------------------------
#  2017-04-27 Mozic Danijel SGD AS-ER/ENN1 * (g40716)
#             \sgdcc_eps_vcc_maintrack\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\4
# --------------------------------------------------------------------------------------------------
#  2017-07-01 RameshKumar RameenaSri CMT EXT (gx1591)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\1
# --------------------------------------------------------------------------------------------------
#  2017-10-11 Cheriyillath Anil CMT EXT (gx2703)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\2
# --------------------------------------------------------------------------------------------------
#  2017-10-19 Grupp Martin SGD AS-ER/ENN2 (g52086)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\Honda_TY3S_Level0\1
# --------------------------------------------------------------------------------------------------
#  2017-10-19 Grupp Martin SGD AS-ER/ENN2 (g52086)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\3
# --------------------------------------------------------------------------------------------------
#  2017-10-26 Grupp Martin SGD AS-ER/ENN2 (g52086)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\4
# --------------------------------------------------------------------------------------------------
#  2017-12-08 Grupp Martin SGD AS-ER/ENN2 (g52086)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\5
# --------------------------------------------------------------------------------------------------
#  2017-12-08 Grupp Martin SGD AS-ER/ENN2 (g52086)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\6
# --------------------------------------------------------------------------------------------------
#  2017-12-12 P Viswanathan Aravind CMT EXT (gbx0068)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\7
# --------------------------------------------------------------------------------------------------
#  2018-01-30 Cheriyillath Anil CMT EXT (gx2703)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\8
# --------------------------------------------------------------------------------------------------
#  2018-02-06 Grupp Martin SGD AS-ER/ENN2 (g52086)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\9
# --------------------------------------------------------------------------------------------------
#  2019-01-30 Beiermeister Markus (AS-ER/ENN2) (EME1SGM)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\17
# --------------------------------------------------------------------------------------------------
#  2018-07-12 P Viswanathan Aravind CMT EXT (gbx0068)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\14
# --------------------------------------------------------------------------------------------------
#  2018-08-14 P Viswanathan Aravind CMT EXT (gbx0068)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\15
# --------------------------------------------------------------------------------------------------
#  2018-11-30 --- (APV5COB)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\16
# --------------------------------------------------------------------------------------------------
#  2019-03-14 Grupp Martin (AS-ER/ENN2) (gmp1sgm)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\18
# --------------------------------------------------------------------------------------------------
#  2019-03-22 --- (apv5cob)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\19
# --------------------------------------------------------------------------------------------------
#  2019-04-03 Sedivec Ondrej (AS-ER/ENN2) (SEO2SGM)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\21
# --------------------------------------------------------------------------------------------------
#  2019-04-05 Sedivec Ondrej (AS-ER/ENN2) (SEO2SGM)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\23
# --------------------------------------------------------------------------------------------------
#  2019-06-05 --- (AHR4COB)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\V2_SDK_Integration_19_02\1
# --------------------------------------------------------------------------------------------------
#  2019-07-01 --- (kkt5cob)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\24
# --------------------------------------------------------------------------------------------------
#  2019-08-01 --- (apv5cob)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\25
# --------------------------------------------------------------------------------------------------
#  2019-08-14 Sedivec Ondrej (AS-ER/ENN2) (SEO2SGM)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves.mak@@\main\26
# --------------------------------------------------------------------------------------------------
# **************************************************************************.HE*********************
