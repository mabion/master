* $ZFLSHEADER$ **************************************************************** 
* ***************************************************************************** 
*                                                                            
*  Copyright (c) Robert Bosch Automotive Steering GmbH, Germany                                
*                All rights reserved                                         
*                                                                            
* ***************************************************************************** 
*                                                                            
*   Dateiname:       AdasMode.kon
* 
*   Zustaendigkeit:  Markus Beiermeister
* 
*   Beschreibung:    Application parameter for the Software Component                             
*                                                                            
* *****************************************************************************

FESTWERT  mAdas_TbtMonTorqueThres_XDU16
  WERT  20
END

FESTWERT  mAdas_TbtMonIntegralThres_XDU32
  WERT  0.2
END

FESTWERT  vAdas_VehicleSpeedMax_XDU16
  WERT  136
END

FESTWERT  xAdas_FadeGradientDown_XDU16
  WERT  0.01
END

FESTWERT  xAdas_FadeGradientUp_XDU16
  WERT  0.01
END

FESTWERT  xAdas_FadeGradientError_XDU16
  WERT  1
END

FESTWERT  xAdas_ActiveReturnReduction_XDU16
  WERT  1
END

FESTWERT fAdas_EnableNominalRackPositionDerivative_XDU8
  WERT  0
END

FESTWERT qAdas_FiltFreqNominalRackPositionForDerivative_XDU16
  WERT  150
END

FESTWERT vAdas_MaxGradRackSpeed_XDU16
  WERT  5
END

FESTWERT xAdas_RackPosCtrlI_XDF32 
   WERT 0.20000
END

FESTWERT  xAdas_RackPosCtrlP_XDF32
  WERT  8.5
END

FESTWERT  xAdas_RackSpeedCtrlP_XDF32
   WERT 0.07999999821186066
END

FESTWERT  xAdas_RackSpeedCtrlI_XDF32
   WERT 1.3999999761581421
END

FESTWERT  mAdas_NominalMotorTorqueIMax_XDF32
   WERT 1.5000000000000000
END

FESTWERT  fAdas_EnableNominalMotorTorqueFilter_XDU8
   WERT 0
END

FESTWERT  xAdas_TBT2MotorTorque_XDU16
   WERT 0.1074
END

KENNLINIE lAdas_MaxRackPositionDepOnVehSpeed_XAU16 9
   ST/X   10 30 40 50 60 70 90 110 130
   WERT   90 90 90 90 90 90 90  90  90
END

KENNLINIE vAdas_MaxRackSpeedDepOnVehSpeed_XAU16 9
   ST/X    0 10 20 30 50 80 100 130 135  
   WERT   51 45 24 16 11  8   7   6   6    
END

FESTWERT fAdas_EnableFeedForwardStaticRackForce_XDU8
   WERT 1.000
END

KENNFELD mAdas_StaticRackForceFF_XAU16 5 4
   ST/X   0.0000   20.0000   40.0000   80.0000   120.0000   
   ST/Y   0.0000
   WERT   0.000000   0.000000   0.000000   0.000000   0.000000   
   ST/Y   3.0000
   WERT   0.000000   0.299805   0.400391   0.599609   0.599609   
   ST/Y   10.0000
   WERT   0.000000   0.500000   0.799805   0.799805   0.799805   
   ST/Y   20.0000
   WERT   0.000000   0.799805   0.799805   0.799805   0.799805  
END

KENNLINIE mAdas_MaxAdasMotorTorqueDepOnVehicleSpeed_XAU16 9
   ST/X   0.0000   2.0000   10.0000   30.0000   50.0000   80.0000   
   ST/X   100.0000   130.0000   150.0000   
   WERT   3.519531   1.500000   1.500000   1.450195   1.099609   1.000000   
   WERT   0.900391   0.799805   0.799805
END

FESTWERT  vAdas_RackSpeedMonTolerance_XDU16
   WERT 100
END

FESTWERT vAdas_NominalRackSpeedIMax_XDF32 
   WERT 0.2000000029802322
END

* **************************************************************************************************
* HISTORY:
* --------
* 
* --------------------------------------------------------------------------------------------------
* Unter anderem: Endekennung fuer neuen KM-Info-Trigger eingef�gt
*  2016-12-19 Gross Marcel SGD AS-ER/ENM2 * (g52690)
*             \sgdcc_eps_sdk3\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves_var_05\ActiveDamping.kon@@\main\1
* --------------------------------------------------------------------------------------------------
*  2017-03-15 Mozic Danijel SGD AS-ER/ENN1 * (g40716)
*             \sgdcc_eps_vcc_maintrack\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves_var_05\ActiveDamping.kon@@\main\1
* --------------------------------------------------------------------------------------------------
*  2017-10-19 Grupp Martin SGD AS-ER/ENN2 (g52086)
*             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves_var_01\ActiveDamping.kon@@\main\Honda_TY3S_Level0\1
* --------------------------------------------------------------------------------------------------
*  2017-10-19 Grupp Martin SGD AS-ER/ENN2 (g52086)
*             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves_var_01\ActiveDamping.kon@@\main\4
* --------------------------------------------------------------------------------------------------
*  2018-02-09 Cheriyillath Anil CMT EXT (gx2703)
*             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves_var_01\ActiveDamping.kon@@\main\5
* --------------------------------------------------------------------------------------------------
*  2018-03-14 Cheriyillath Anil CMT EXT (gx2703)
*             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves_var_01\ActiveDamping.kon@@\main\6
* --------------------------------------------------------------------------------------------------
*  2018-04-24 P Viswanathan Aravind CMT EXT (gbx0068)
*             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves_var_01\ActiveDamping.kon@@\main\7
* --------------------------------------------------------------------------------------------------
*  2019-01-30 Beiermeister Markus (AS-ER/ENN2) (EME1SGM)
*             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves_var_01\AdasMode.kon@@\main\1
* --------------------------------------------------------------------------------------------------
*  2019-02-06 --- (MNE1COB)
*             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves_var_01\AdasMode.kon@@\main\2
* --------------------------------------------------------------------------------------------------
*  2019-03-15 Beiermeister Markus (AS-ER/ENN2) (EME1SGM)
*             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves_var_01\AdasMode.kon@@\main\3
* --------------------------------------------------------------------------------------------------
*  2019-04-17 Beiermeister Markus (AS-ER/ENN2) (EME1SGM)
*             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves_var_01\AdasMode.kon@@\main\4
* --------------------------------------------------------------------------------------------------
*  2019-04-27 --- (apv5cob)
*             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves_var_01\AdasMode.kon@@\main\5
* --------------------------------------------------------------------------------------------------
*  2019-06-06 --- (kkt5cob)
*             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves_var_01\AdasMode.kon@@\main\6
* --------------------------------------------------------------------------------------------------
*  2019-07-19 Beiermeister Markus (AS-ER/ENN2) (EME1SGM)
*             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves_var_01\AdasMode.kon@@\main\Special_Branch_Quality_Rework\1
* --------------------------------------------------------------------------------------------------
*  2019-08-01 --- (apv5cob)
*             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves_var_01\AdasMode.kon@@\main\7
* --------------------------------------------------------------------------------------------------
*  2019-08-08 --- (kkt5cob)
*             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Preserves\Preserves_var_01\AdasMode.kon@@\main\8
* --------------------------------------------------------------------------------------------------
* **************************************************************************.HE*********************
