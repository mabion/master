# $ZFLSHEADER$ ----------------------------------------------------------------
# -----------------------------------------------------------------------------  
#                                                                            
#  Copyright (c) ZF Lenksysteme GmbH, Germany                                
#                All rights reserved                                         
#                                                                            
# ----------------------------------------------------------------------------- 
#                                                                            
#   Dateiname:       MechData.mak
# 
#   Beschreibung:    MechData makefile
#                                                                                                    
# -----------------------------------------------------------------------------                                                                        

FEAT = FEAT_DEV_MECHDATA

$(FEAT)_KON_FILES += $(DAMOS_TEMP_PATH)\mech.kon 



# $END_ZFLSHEADER$ ------------------------------------------------------------ 



# **************************************************************************************************
# HISTORY:
# --------
#  2008-03-05 Boensch, Michael (G51827)
#             \steering_epsdaimlerc218w166\SW\ImplementationSet\Infrastructure\GlobalData\SystemData\SystemData.mak@@\main\1
# --------------------------------------------------------------------------------------------------
#  2008-06-03 Boensch, Michael (G51827)
#             \steering_epsdaimlerc218w166\SW\ImplementationSet\Infrastructure\GlobalData\SystemData\SystemData.mak@@\main\2
# --------------------------------------------------------------------------------------------------
#  2008-06-26 Boensch, Michael (G51827)
#             \steering_epsdaimlerc218w166\SW\ImplementationSet\Infrastructure\GlobalData\MechData\MechData.mak@@\main\1
# --------------------------------------------------------------------------------------------------
#  2009-12-09 Berckhemer Uwe SGD EZPA * (g52672)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MechData\MechData.mak@@\main\1
# --------------------------------------------------------------------------------------------------
#  2010-04-09 Mergenthaler Thomas SGD EZPA * (g52484)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MechData\MechData.mak@@\main\2
# --------------------------------------------------------------------------------------------------
#  2017-07-01 RameshKumar RameenaSri CMT EXT (gx1591)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\MechData\MechData.mak@@\main\1
# --------------------------------------------------------------------------------------------------
# **************************************************************************.HE*********************
