/* $ZFLSHEADER$ *************************************************************
    Copyright (c) ZF Lenksysteme GmbH, Germany
                  All rights reserved
   *************************************************************************/

/*! \file
 * \brief Stubs f�r Funktionsaufrufe und Variablendefinitionen, die in andere
 *        Dateien verschoben werden m�ssen.
 */

#include "ram_zfls_gg.h"

#include "damos_ram_zfls_gg.c"



// **************************************************************************************************
// HISTORY:
// --------
// 2007-11-15 Herter, Mark (g52700)
//            \steering_epsdaimlerc218w166\SW\ImplementationSet\ram_zfls_gg.c@@\main\1
// --------------------------------------------------------------------------------------------------
// 2008-02-21 Haas, Bernd (g51950)
//            \steering_epsdaimlerc218w166\SW\ImplementationSet\ram_zfls_gg.c@@\main\2
// --------------------------------------------------------------------------------------------------
// 2008-05-28 Haas, Bernd (g51950)
//            \steering_epsdaimlerc218w166\SW\ImplementationSet\ram_zfls_gg.c@@\main\3
// --------------------------------------------------------------------------------------------------
// 2009-12-09 Berckhemer Uwe SGD EZPA * (g52672)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\mcdata\ram_zfls_gg.c@@\main\1
// --------------------------------------------------------------------------------------------------
// 2010-12-13 Gruenenwald Andreas SGD EXT (gx0630)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\ram_zfls_gg.c@@\main\int_wp_job27320\1
// --------------------------------------------------------------------------------------------------
// 2011-01-19 Klotz Dietmar SGD EXT (gx0838)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\ram_zfls_gg.c@@\main\2
// --------------------------------------------------------------------------------------------------
// 2012-01-30 Rieker Michael SGD EZPA (g53199)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\ram_zfls_gg.c@@\main\3
// --------------------------------------------------------------------------------------------------
// 2014-06-30 --- (g52975)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\ram_zfls_gg.c@@\main\int_var_sdk22\int_wp_job63474\1
// --------------------------------------------------------------------------------------------------
// 2014-07-25 --- (gx1478)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\ram_zfls_gg.c@@\main\int_var_sdk22\1
// --------------------------------------------------------------------------------------------------
// 2015-01-21 --- (gx1478)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\ram_zfls_gg.c@@\main\4
// --------------------------------------------------------------------------------------------------
// 2017-08-25 Halavart Cafer SGD AS-ER/ENK2 (g53166)
//            \sgdcc_eps_sdk3\EpsDriveSW\ImplementationSet\DataContainer\MCData\ram_zfls_gg.c@@\main\int_wp_job144246\1
// --------------------------------------------------------------------------------------------------
// 2017-09-04 RameshKumar RameenaSri CMT EXT (gx1591)
//            \sgdcc_eps_sdk3\EpsDriveSW\ImplementationSet\DataContainer\MCData\ram_zfls_gg.c@@\main\2
// --------------------------------------------------------------------------------------------------
// 2017-11-14 Mozic Danijel SGD AS-ER/ENN1 * (g40716)
//            \sgdcc_eps_vcc_maintrack\EpsDriveSW\ImplementationSet\DataContainer\MCData\ram_zfls_gg.c@@\main\2
// --------------------------------------------------------------------------------------------------
// 2018-01-30 Cheriyillath Anil CMT EXT (gx2703)
//            \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\MCData\ram_zfls_gg.c@@\main\2
// --------------------------------------------------------------------------------------------------
// **************************************************************************.HE*********************
