/* $ZFLSHEADER$ *************************************************************
    Copyright (c) ZF Lenksysteme GmbH, Germany
                  All rights reserved
   *************************************************************************/

/*! \file
 * \brief Stubs f�r Funktionsaufrufe und Variablendefinitionen, die in andere
 *        Dateien verschoben werden m�ssen.
 */


// all Application-Parameter must not use SDA !! (therefor: data)
// while tuning they will be mapped to SDA-unreachable global ram

#include "rom_zfls_gg.h"

#include "damos_rom_zfls_gg.c"


// **************************************************************************************************
// HISTORY:
// --------
// 2007-11-15 Herter, Mark (g52700)
//            \steering_epsdaimlerc218w166\SW\ImplementationSet\rom_zfls_gg.c@@\main\1
// --------------------------------------------------------------------------------------------------
// 2008-01-31 Raabe, Siegfried (gx0260)
//            \steering_epsdaimlerc218w166\SW\ImplementationSet\rom_zfls_gg.c@@\main\2
// --------------------------------------------------------------------------------------------------
// 2009-12-09 Berckhemer Uwe SGD EZPA * (g52672)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\mcdata\rom_zfls_gg.c@@\main\1
// --------------------------------------------------------------------------------------------------
// 2017-05-29 Pavel Stefan SGD AS-ER/ENM1 * (g51949)
//            \sgdcc_eps_sdk3\EpsDriveSW\ImplementationSet\DataContainer\MCData\rom_zfls_gg.c@@\main\2
// --------------------------------------------------------------------------------------------------
// 2017-06-29 Halavart Cafer SGD AS-ER/ENK2 (g53166)
//            \sgdcc_eps_sdk3\EpsDriveSW\ImplementationSet\DataContainer\MCData\rom_zfls_gg.c@@\main\int_wp_job144246\1
// --------------------------------------------------------------------------------------------------
// 2017-08-25 Halavart Cafer SGD AS-ER/ENK2 (g53166)
//            \sgdcc_eps_sdk3\EpsDriveSW\ImplementationSet\DataContainer\MCData\rom_zfls_gg.c@@\main\int_wp_job144246\2
// --------------------------------------------------------------------------------------------------
// 2017-09-04 RameshKumar RameenaSri CMT EXT (gx1591)
//            \sgdcc_eps_sdk3\EpsDriveSW\ImplementationSet\DataContainer\MCData\rom_zfls_gg.c@@\main\3
// --------------------------------------------------------------------------------------------------
// 2017-11-14 Mozic Danijel SGD AS-ER/ENN1 * (g40716)
//            \sgdcc_eps_vcc_maintrack\EpsDriveSW\ImplementationSet\DataContainer\MCData\rom_zfls_gg.c@@\main\3
// --------------------------------------------------------------------------------------------------
// 2018-01-30 Cheriyillath Anil CMT EXT (gx2703)
//            \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\MCData\rom_zfls_gg.c@@\main\3
// --------------------------------------------------------------------------------------------------
// **************************************************************************.HE*********************
