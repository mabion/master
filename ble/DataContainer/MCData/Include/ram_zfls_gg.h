/* $ZFLSHEADER$ *************************************************************
    Copyright (c) ZF Lenksysteme GmbH, Germany
                  All rights reserved
   *************************************************************************/

/*! \file
 * \brief Stubs f�r Funktionsaufrufe und Variablendefinitionen, die in andere
 *        Dateien verschoben werden m�ssen.
 */
#ifndef _RAM_GG_H
#define _RAM_GG_H

#include "types_aehcc_hh.h"
#include "IMcDataIntegrity_ctncc_hh.h"
#include "csy_osp_ram_h_zfls.h"


#include "damos_ram_zfls_gg.h"

#if defined (TARGETLINK) || ((VCAR) && (VCAR != DIVTEST)) || (QAC) || (PST)
   // do nothing
#else
	#include "INvmAbstraction.h"
	#include "NvMInterface_Includes.h"
#endif

#endif
// **************************************************************************************************
// HISTORY:
// --------
// 2007-11-15 Herter, Mark (g52700)
//            \steering_epsdaimlerc218w166\SW\ImplementationSet\Include\ram_zfls_gg.h@@\main\1
// --------------------------------------------------------------------------------------------------
// 2007-11-20 Herter, Mark (g52700)
//            \steering_epsdaimlerc218w166\SW\ImplementationSet\Include\ram_zfls_gg.h@@\main\2
// --------------------------------------------------------------------------------------------------
// 2008-04-22 Herter, Mark (g52700)
//            \steering_epsdaimlerc218w166\SW\ImplementationSet\Include\ram_zfls_gg.h@@\main\3
// --------------------------------------------------------------------------------------------------
// 2009-06-22 Herter, Mark (g52700)
//            \steering_epsdaimlerc218w166\SW\ImplementationSet\Include\ram_zfls_gg.h@@\main\4
// --------------------------------------------------------------------------------------------------
// 2010-02-25 --- (gx0837)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\Include\ram_zfls_gg.h@@\main\1
// --------------------------------------------------------------------------------------------------
// 2010-08-10 Klotz Dietmar SGD EXT (gx0838)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\Include\ram_zfls_gg.h@@\main\int_wp_job18255\WPR19168_Pre_Integration\1
// --------------------------------------------------------------------------------------------------
// 2010-08-10 Klotz Dietmar SGD EXT (gx0838)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\Include\ram_zfls_gg.h@@\main\int_wp_job18255\1
// --------------------------------------------------------------------------------------------------
// 2010-09-07 Klotz Dietmar SGD EXT (gx0838)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\Include\ram_zfls_gg.h@@\main\2
// --------------------------------------------------------------------------------------------------
// 2010-11-23 Geiger Florian SGD EZPB (g53032)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\Include\ram_zfls_gg.h@@\main\int_wp_job23163\1
// --------------------------------------------------------------------------------------------------
// 2010-12-03 Klotz Dietmar SGD EXT (gx0838)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\Include\ram_zfls_gg.h@@\main\3
// --------------------------------------------------------------------------------------------------
// 2013-08-13 --- (g51902)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\ram_zfls_gg.h@@\main\int_var_sdk22\int_wp_job57549\1
// --------------------------------------------------------------------------------------------------
// 2013-09-05 --- (gx1478)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\ram_zfls_gg.h@@\main\int_var_sdk22\1
// --------------------------------------------------------------------------------------------------
// 2015-01-21 --- (gx1478)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\ram_zfls_gg.h@@\main\4
// --------------------------------------------------------------------------------------------------
// 2015-08-13 Sutter Michael SGD PC-DESA * (g51902)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\ram_zfls_gg.h@@\main\int_var_sdk234\int_wp_95181\1
// --------------------------------------------------------------------------------------------------
// 2015-08-20 Halavart Cafer SGD EXT (gx1487)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\ram_zfls_gg.h@@\main\int_var_sdk234\1
// --------------------------------------------------------------------------------------------------
// 2015-10-09 Halavart Cafer SGD EXT (gx1487)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\ram_zfls_gg.h@@\main\5
// --------------------------------------------------------------------------------------------------
// 2015-11-27 Sutter Michael SGD PC-DSSA * (g51902)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\ram_zfls_gg.h@@\main\6
// --------------------------------------------------------------------------------------------------
// 2015-12-11 Haug Fabian SGD PC-DSSA * (g52267)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\ram_zfls_gg.h@@\main\7
// --------------------------------------------------------------------------------------------------
// 2016-02-04 Vaas Rainer SGD PC-DSSA (g52975)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\ram_zfls_gg.h@@\main\int_wp_job98020\1
// --------------------------------------------------------------------------------------------------
// 2016-03-09 Schaal Thomas SGD EXT (gx1478)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\ram_zfls_gg.h@@\main\8
// --------------------------------------------------------------------------------------------------
// 2016-04-01 Vaas Rainer SGD PC-DSSA (g52975)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\ram_zfls_gg.h@@\main\int_wp_job98024_2\1
// --------------------------------------------------------------------------------------------------
// 2016-04-13 Schaal Thomas SGD EXT (gx1478)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\ram_zfls_gg.h@@\main\9
// --------------------------------------------------------------------------------------------------
// 2017-11-14 Mozic Danijel SGD AS-ER/ENN1 * (g40716)
//            \sgdcc_eps_vcc_maintrack\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\ram_zfls_gg.h@@\main\2
// --------------------------------------------------------------------------------------------------
// 2018-01-30 Cheriyillath Anil CMT EXT (gx2703)
//            \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\ram_zfls_gg.h@@\main\2
// --------------------------------------------------------------------------------------------------
// 2018-03-29 Kamga Wilfried SGD AS-ER/ENN2 (G53880)
//            \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\ram_zfls_gg.h@@\main\3
// --------------------------------------------------------------------------------------------------
// 2018-06-28 Arojojoye Ayodele SGD AS-ER/ENN2 (g53494)
//            \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\ram_zfls_gg.h@@\main\175771_SDK_Update_wrt_Volvo\1
// --------------------------------------------------------------------------------------------------
// 2018-07-02 P Viswanathan Aravind CMT EXT (gbx0068)
//            \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\ram_zfls_gg.h@@\main\4
// --------------------------------------------------------------------------------------------------
// **************************************************************************.HE*********************
