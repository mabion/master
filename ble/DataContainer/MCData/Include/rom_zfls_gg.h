/* $ZFLSHEADER$ *************************************************************
    Copyright (c) ZF Lenksysteme GmbH, Germany
                  All rights reserved
   *************************************************************************/

/*! \file
 * \brief Stubs f�r Funktionsaufrufe und Variablendefinitionen, die in andere
 *        Dateien verschoben werden m�ssen.
 */

#ifndef _ROM_GG_H
#define _ROM_GG_H

#include "types_aehcc_hh.h"
#include "IMathLibrary_cqicc_ii.h"
#include "csy_osp_rom_h_zfls.h"

// all Application-Parameter must not use SDA !! (therefor: data)
// while tuning they will be mapped to SDA-unreachable global ram
  #pragma ghs startsda
  #include "damos_rom_zfls_gg.h"
  #pragma ghs endsda

#endif

// **************************************************************************************************
// HISTORY:
// --------
// 2007-11-15 Herter, Mark (g52700)
//            \steering_epsdaimlerc218w166\SW\ImplementationSet\Include\rom_zfls_gg.h@@\main\1
// --------------------------------------------------------------------------------------------------
// 2007-11-20 Herter, Mark (g52700)
//            \steering_epsdaimlerc218w166\SW\ImplementationSet\Include\rom_zfls_gg.h@@\main\2
// --------------------------------------------------------------------------------------------------
// 2008-04-22 Herter, Mark (g52700)
//            \steering_epsdaimlerc218w166\SW\ImplementationSet\Include\rom_zfls_gg.h@@\main\3
// --------------------------------------------------------------------------------------------------
// 2009-06-22 Herter, Mark (g52700)
//            \steering_epsdaimlerc218w166\SW\ImplementationSet\Include\rom_zfls_gg.h@@\main\4
// --------------------------------------------------------------------------------------------------
// 2010-02-25 --- (gx0837)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\Include\rom_zfls_gg.h@@\main\1
// --------------------------------------------------------------------------------------------------
// 2013-08-13 --- (g51902)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\rom_zfls_gg.h@@\main\int_var_sdk22\int_wp_job57549\1
// --------------------------------------------------------------------------------------------------
// 2013-09-05 --- (gx1478)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\rom_zfls_gg.h@@\main\int_var_sdk22\1
// --------------------------------------------------------------------------------------------------
// 2015-01-21 --- (gx1478)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\rom_zfls_gg.h@@\main\2
// --------------------------------------------------------------------------------------------------
// 2015-08-13 Sutter Michael SGD PC-DESA * (g51902)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\rom_zfls_gg.h@@\main\int_var_sdk234\int_wp_95181\1
// --------------------------------------------------------------------------------------------------
// 2015-08-20 Halavart Cafer SGD EXT (gx1487)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\rom_zfls_gg.h@@\main\int_var_sdk234\1
// --------------------------------------------------------------------------------------------------
// 2015-10-09 Halavart Cafer SGD EXT (gx1487)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\rom_zfls_gg.h@@\main\3
// --------------------------------------------------------------------------------------------------
// 2015-12-11 Haug Fabian SGD PC-DSSA * (g52267)
//            \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\rom_zfls_gg.h@@\main\4
// --------------------------------------------------------------------------------------------------
// 2017-08-15 Mozic Danijel SGD AS-ER/ENN1 * (g40716)
//            \sgdcc_eps_vcc_maintrack\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\rom_zfls_gg.h@@\main\2
// --------------------------------------------------------------------------------------------------
// 2017-11-14 Mozic Danijel SGD AS-ER/ENN1 * (g40716)
//            \sgdcc_eps_vcc_maintrack\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\rom_zfls_gg.h@@\main\3
// --------------------------------------------------------------------------------------------------
// 2018-01-30 Cheriyillath Anil CMT EXT (gx2703)
//            \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\MCData\Include\rom_zfls_gg.h@@\main\3
// --------------------------------------------------------------------------------------------------
// **************************************************************************.HE*********************
