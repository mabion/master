# $ZFLSHEADER$ ----------------------------------------------------------------
# -----------------------------------------------------------------------------
#
#  Copyright (c) ZF Lenksysteme GmbH, Germany
#                All rights reserved
#
# -----------------------------------------------------------------------------

FEAT = FEAT_DEV_MCDATA

$(FEAT)_C_FILES     += $(CPR_SRC_BASE)\datacontainer\mcdata\ram_zfls_gg.c
$(FEAT)_C_FILES     += $(CPR_SRC_BASE)\datacontainer\mcdata\rom_zfls_gg.c

$(FEAT)_INCLUDE_DIR += $(CPR_SRC_BASE)\datacontainer\mcdata\Include

#remove compiler size and linker optimizations
ram_zfls_gg_CFLAGS = $(CFLAGS,S'-Osize'-Onone',S'-Olink'-Onolink')
rom_zfls_gg_CFLAGS = $(CFLAGS,S'-Osize'-Onone',S'-Olink'-Onolink')

#remove compiler size, loop and linker optimizations
damos_ram_DF08_CFLAGS = $(CFLAGS,S'-Osize'-Onone',S'-Oloop'-Onone',S'-Olink'-Onone')


# **************************************************************************************************
# HISTORY:
# --------
#
#  2009-12-09 Berckhemer Uwe SGD EZPA * (g52672)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\mcdata\MCData.mak@@\main\1
# --------------------------------------------------------------------------------------------------
#  2010-04-09 Mergenthaler Thomas SGD EZPA * (g52484)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\mcdata\MCData.mak@@\main\2
# --------------------------------------------------------------------------------------------------
#  2011-07-15 Gruenenwald Andreas SGD EXT (gx0630)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\MCData.mak@@\main\int_wp_job34172\1
# --------------------------------------------------------------------------------------------------
#  2011-07-20 Klotz Dietmar SGD EXT (gx0838)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\MCData.mak@@\main\3
# --------------------------------------------------------------------------------------------------
#  2012-01-30 Rieker Michael SGD EZPA (g53199)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\MCData.mak@@\main\4
# --------------------------------------------------------------------------------------------------
#  2013-02-01 Berckhemer Uwe SGD ZEEPA * (g52672)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\MCData.mak@@\main\5
# --------------------------------------------------------------------------------------------------
#  2014-06-30 --- (g52975)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\MCData.mak@@\main\int_var_sdk22\int_wp_job63474\1
# --------------------------------------------------------------------------------------------------
#  2014-07-25 --- (gx1478)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\MCData.mak@@\main\int_var_sdk22\1
# --------------------------------------------------------------------------------------------------
#  2015-01-21 --- (gx1478)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\MCData.mak@@\main\6
# --------------------------------------------------------------------------------------------------
#  2016-02-04 Vaas Rainer SGD PC-DSSA (g52975)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\MCData.mak@@\main\int_wp_job98020\1
# --------------------------------------------------------------------------------------------------
#  2016-03-09 Schaal Thomas SGD EXT (gx1478)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\MCData.mak@@\main\7
# --------------------------------------------------------------------------------------------------
#  2016-04-18 Sutter Michael SGD PC-DSSA * (g51902)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\MCData.mak@@\main\int_wp_113210\1
# --------------------------------------------------------------------------------------------------
#  2016-04-26 Schaal Thomas SGD EXT (gx1478)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\MCData\MCData.mak@@\main\8
# --------------------------------------------------------------------------------------------------
#  2017-01-25 --- (g52226)
#             \sgdcc_eps_sdk3\EpsDriveSW\ImplementationSet\DataContainer\MCData\MCData.mak@@\main\2
# --------------------------------------------------------------------------------------------------
#  2017-03-15 Mozic Danijel SGD AS-ER/ENN1 * (g40716)
#             \sgdcc_eps_vcc_maintrack\EpsDriveSW\ImplementationSet\DataContainer\MCData\MCData.mak@@\main\2
# --------------------------------------------------------------------------------------------------
#  2017-08-21 Moorthy Sudhakar SGD AS-ER/ENN1 (G54391)
#             \sgdcc_eps_vcc_maintrack\EpsDriveSW\ImplementationSet\DataContainer\MCData\MCData.mak@@\main\5
# --------------------------------------------------------------------------------------------------
#  2017-11-14 Mozic Danijel SGD AS-ER/ENN1 * (g40716)
#             \sgdcc_eps_vcc_maintrack\EpsDriveSW\ImplementationSet\DataContainer\MCData\MCData.mak@@\main\6
# --------------------------------------------------------------------------------------------------
#  2018-01-30 Cheriyillath Anil CMT EXT (gx2703)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\MCData\MCData.mak@@\main\2
# --------------------------------------------------------------------------------------------------
# **************************************************************************.HE*********************
