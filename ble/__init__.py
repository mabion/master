#############################
# START
#############################

dcmPath = "C:\\__ws\\00_ws\\__script\\py\\tunning\\Tuning_Cal_Implementation"
vob_path = "C:\\Users\\tu.phan-ngoc\\Desktop\\Python\\master\\ble\\__fromVob"

def main():
    import dcm_sub_func
    import inter_func
    import log_sub_func
    import kon_sub_func
    import gui

    KonDict = kon_sub_func.kon_pre_loader(vob_path)
    DcmDict = dcm_sub_func.Dcm(vob_path)
    print(KonDict)

    gui.popup(KonDict)

    # log_sub_func.log_header()

    # dcm_tun_list, dcm_other_list, dcm_dict = dcm_sub_func.dcm_var_coded_sorter(
    #     dcm_sub_func.dcm_var_sorter(filter(None, dcm_sub_func.dcm_loader(dcmPath))))

    # inter_func.tun_list_sorter(dcm_tun_list)

    # inter_func.other_list_sorter(dcm_other_list)

    # log_sub_func.shut_log()

    # del dcm_sub_func, inter_func, log_sub_func, gui

main()