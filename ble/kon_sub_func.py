#############################
# START
#############################
from tkinter import StringVar
import parameters, os, log_sub_func

def kon_handler(var_list, kon_file):
    var_name = list(filter(None, var_list[0].split(' ')))[1]

    import glob
    import os
    try:
        fptr = open(glob.glob(os.getcwd() + '\\Kon_files\\' + kon_file)[0], 'r+')
    except IndexError:
        import log_sub_func
        log_sub_func.log_msg(parameters.insig_tag + 'Kon file ' + kon_file +
                             ' not provided. Hence creating a new file with that name.')

        fptr = open(os.getcwd() + '\\Kon_files\\' + kon_file, 'w')
        del log_sub_func
    del glob, os

    try:
        kon_lines = fptr.read().splitlines()
    except IOError:
        kon_lines = []

    if var_name in get_kon_vars(kon_lines):
        kon_lines = kon_update(var_list, kon_lines)
        status = '0'
    else:
        kon_lines = kon_add(var_list, kon_lines)
        status = '1'

    fptr.seek(0)
    fptr.truncate()

    for line in kon_lines:
        fptr.write(line + '\n')

    fptr.close()
    return fptr.name + ';' + status

def kon_handler_mabion(var_list, kon_file):
    var_name = list(filter(None, var_list[0].split(' ')))[1]

    import glob
    import os
    try:
        fptr = open(kon_file, 'r+')
    except IndexError:
        import log_sub_func
        log_sub_func.log_msg(parameters.insig_tag + kon_file +
                             ' not provided. Hence creating a new file with that name.')

        fptr = open(kon_file, 'w')
        del log_sub_func
    del glob, os

    try:
        kon_lines = fptr.read().splitlines()
    except IOError:
        kon_lines = []

    if var_name in get_kon_vars(kon_lines):
        kon_lines = kon_update(var_list, kon_lines)
        status = '0'
    else:
        kon_lines = kon_add(var_list, kon_lines)
        status = '1'

    fptr.seek(0)
    fptr.truncate()

    for line in kon_lines:
        fptr.write(line + '\n')

    fptr.close()
    return fptr.name + ';' + status

def get_kon_vars(temp_lines):

    var_list = []
    for line in temp_lines:
        for var_type in parameters.var_types:
            if var_type in line:
                var_list.append(filter(None, line.split(' '))[1])

    return var_list


def kon_update(var_list, kon_lines):
    var_name = filter(None, var_list[0].split(' '))[1]

    i = 0

    for i in range(len(kon_lines)):
        if var_name in kon_lines[i]:
            break

    kon_left = kon_lines[:i]

    temp_slice = kon_lines[i:]

    for i in range(len(temp_slice)):
        if 'END' in temp_slice[i]:
            break

    kon_right = temp_slice[i+1:]
    kon_lines = kon_left + var_list + kon_right

    return kon_lines


def kon_add(var_list, kon_lines):
    end_count = 0
    end_index = len(kon_lines)
    for i in range(len(kon_lines)):
        if 'END' == kon_lines[i]:
            end_count += 1
            end_index = i

    kon_left = kon_lines[:end_index + 1]
    kon_right = kon_lines[end_index + 1:]

    kon_lines = kon_left + [' '] + var_list + kon_right

    return kon_lines


def kon_change_mode(DataContainerPath):
    import os
    from stat import S_IREAD, S_IRGRP, S_IROTH

    # for file in os.listdir(DataContainerPath):
    #     if file.endswith(".kon"):
    #         kon_files.append(os.path.join(inPath, file))

    # for root, subdirs, files in os.walk(rootdir):
    #     os.path.join(root, 'my-directory-list.txt')
    #     os.chmod(filename, S_IREAD|S_IRGRP|S_IROTH)
        
    #     for filename in files:
    #         file_path = os.path.join(root, filename)


class kon_error:
    def __init__(self):
        self.Info = []
        self.Cnt = 0
    def Set(self, type, describe):
        self.Cnt += 1
        self.Info.append(type + ":\t" + describe)
class kon_attribute():
    def __init__(self, name, path):
        self.Name = name
        self.Path = path
        self.Error = kon_error()
    def SetError(self, type, describe):
        self.Error.Set(type, describe)
class kon_pre_loader():
    def __init__(self, Vob_Path):
        self.KonDict = self.DictHandle(Vob_Path + "\\DataContainer")

    def GetKonDict(self):
        return self.KonDict

    def DictHandle(self, path):
        KonDict = {}
        fileslist = parameters.ListFile(path, (".kon", ".KON", ".Kon"))
        for kon in fileslist:
            koninfo = list(filter(None, (kon.replace("\\","/")).split('/')))
            KonDict[koninfo[-2] +"/" +koninfo[-1]] = kon_attribute(koninfo[-1], kon)
        return KonDict

