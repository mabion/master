#############################
# START
#############################
import parameters
import datetime


def log_header():

    dt = datetime.datetime.now()

    import getpass
    user = getpass.getuser()
    del getpass

    dateline = dt.strftime("%H:%M:%S") + " "
    log_msg(parameters.insig_tag + 'Initiated at ' + dateline + 'by ' + user + '....')
    return


def log_msg(message):
    parameters.log_file.write(message + '\n')
    return


def shut_log():
    dt = datetime.datetime.now()
    time_stamp = parameters.insig_tag + 'Terminating at ' + dt.strftime("%H:%M.%S") + ", " + str(dt.day) + 'th ' + \
                 str(dt.strftime("%B")) + ' ' + str(dt.year)
    log_msg(time_stamp)
    parameters.log_file.close()

    if parameters.tracing.lower() == 'off':
        import os
        os.remove('trace.log')
        del os

    return
