import tkinter as tk


class app(tk.Frame):

    def __init__(self, parent,*args,**kwargs):
        tk.Frame.__init__(self, parent,*args,**kwargs)
        self.parent=parent

        toggler = tk.Button(self,text='DISABLE OUTER',command=self.toggleMain)
        toggler.pack(padx=20,pady=20)

    def toggleMain(self):

        status= self.parent.grab_status()
        if not status:
            print('Outer Is Disabled (Hi Should not work)')
            self.grab_set()
        else:
            print('Outer is Enabled (Hi should work)')
            self.grab_release()

root = tk.Tk()

app = app(root,bg='red')
app.pack()


def SayHi():
    print('Hi There')

f=tk.Button(root,text='Say Hi',command=SayHi)
f.pack()

root.mainloop()