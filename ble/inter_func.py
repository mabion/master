#############################
# START
#############################
import parameters
import re
import kon_sub_func
import log_sub_func


def tun_list_sorter(tun_list):

    log_sub_func.log_msg(parameters.insig_tag + 'Working on the variant coded variables...')

    for var in tun_list:

        status = 'No tuning kon found!'
        var_name = var[0].split(' ')[1]

        if '_0_' in var[0]:
            status = kon_sub_func.kon_handler(var, parameters.tun_dict['_0_'])

        elif '_1_' in var[0]:

            var[0] = re.sub("_1_", '_0_', var[0])
            status = kon_sub_func.kon_handler(var, parameters.tun_dict['_1_'])

        elif '_2_' in var[0]:
            var[0] = re.sub("_2_", '_0_', var[0])
            status = kon_sub_func.kon_handler(var, parameters.tun_dict['_2_'])

        elif '_3_' in var[0]:
            var[0] = re.sub("_3_", '_0_', var[0])
            status = kon_sub_func.kon_handler(var, parameters.tun_dict['_3_'])

        log_sub_func.log_msg(var_name + ';' + status)

    log_sub_func.log_msg(parameters.insig_tag + 'Done...')
    return


def tun_dict_sorter(tun_dict, SelectedMapset, SelectedKonFiles):

    log_sub_func.log_msg(parameters.insig_tag + 'Working on the variant coded variables...')

    for konfile in SelectedKonFiles:
        kon_sub_func.kon_handler_mabion(tun_dict[SelectedMapset], konfile) # kon_handler should be custom as mabion requirement

    log_sub_func.log_msg(parameters.insig_tag + 'Done...')
    return

def other_dict_sorter(tun_dict, SelectedMapset, SelectedKonFiles):
    
    log_sub_func.log_msg(parameters.insig_tag + 'Working on the variant coded variables...')

    for konfile in SelectedKonFiles:
        kon_sub_func.kon_handler(tun_dict[SelectedMapset], konfile) # kon_handler should be custom as mabion requirement

    log_sub_func.log_msg(parameters.insig_tag + 'Done...')
    return



def other_list_sorter(other_list):
    log_sub_func.log_msg(parameters.insig_tag + 'Working on the non - variant coded variables...')

    for var in other_list:
        var_name = var[0].split(' ')[1]
        result = 1
        i = 0
        while result == 1:
            result, status = process_var(var, parameters.non_var_coded_mode[i])
            if result == 0:
                log_sub_func.log_msg(var_name + ';' + status)
                break
            i += 1

    log_sub_func.log_msg(parameters.insig_tag + 'Done...')
    return


def process_var(var_list, mode):
    result = 1
    function = {'FUNKTION': 'NA'}
    status = ''
    var_name = var_list[0].split(' ')[1]

    if type(var_list[-1]) == dict:
        function = var_list.pop(-1)

    if mode == 0:
        for key in parameters.funct_dict.keys():
            if function['FUNKTION'] == key:
                status = kon_sub_func.kon_handler(var_list, parameters.funct_dict[key])
                result = 0

    if mode == 1:
        for key in parameters.key_dict.keys():
            if key in var_list[0]:
                status = kon_sub_func.kon_handler(var_list, parameters.key_dict[key])
                result = 0

    if mode == 2:
        import glob
        import os
        for kon_file in glob.glob(os.getcwd() + '\\Kon_Files\\*.kon'):
            temp_ptr = open(kon_file, 'r+')
            temp_lines = temp_ptr.read().splitlines()
            kon_var_list = kon_sub_func.get_kon_vars(temp_lines)
            if var_name in kon_var_list:
                status = kon_sub_func.kon_handler(var_list, kon_file.split('\\')[-1])
                result = 0

        del glob, os

    if mode == 10:
        status = kon_sub_func.kon_handler(var_list, parameters.common_file_name)
        result = 0

    if mode == 11:
        status = kon_sub_func.kon_handler(var_list, parameters.temp_file_name)
        result = 0

    return result, status
