# $ZFLSHEADER$ ----------------------------------------------------------------
# -----------------------------------------------------------------------------  
#                                                                            
#  Copyright (c) ZF Lenksysteme GmbH, Germany                                
#                All rights reserved                                         
#                                                                            
# ----------------------------------------------------------------------------- 
#                                                                            
#   Dateiname:       SystemData.mak
# 
#   Beschreibung:    SystemData makefile
#                                                                                                    
# -----------------------------------------------------------------------------                                                                           

FEAT = FEAT_DEV_SYSTEMDATA

$(FEAT)_KON_FILES += $(DAMOS_TEMP_PATH)\system.kon 
   
# $END_ZFLSHEADER$ ------------------------------------------------------------ 



# **************************************************************************************************
# HISTORY:
# --------
#  2008-03-05 Boensch, Michael (G51827)
#             \steering_epsdaimlerc218w166\SW\ImplementationSet\Infrastructure\GlobalData\SystemData\SystemData.mak@@\main\1
# --------------------------------------------------------------------------------------------------
#  2008-06-03 Boensch, Michael (G51827)
#             \steering_epsdaimlerc218w166\SW\ImplementationSet\Infrastructure\GlobalData\Tuning\Tuning.mak@@\main\1
# --------------------------------------------------------------------------------------------------
#  2008-06-26 Boensch, Michael (G51827)
#             \steering_epsdaimlerc218w166\SW\ImplementationSet\Infrastructure\GlobalData\Tuning\Tuning.mak@@\main\2
# --------------------------------------------------------------------------------------------------
#  2009-12-09 Berckhemer Uwe SGD EZPA * (g52672)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Tuning\Tuning.mak@@\main\1
# --------------------------------------------------------------------------------------------------
#  2010-04-09 Mergenthaler Thomas SGD EZPA * (g52484)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\SystemData\SystemData.mak@@\main\1
# --------------------------------------------------------------------------------------------------
#  2017-07-01 RameshKumar RameenaSri CMT EXT (gx1591)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\SystemData\SystemData.mak@@\main\1
# --------------------------------------------------------------------------------------------------
# **************************************************************************.HE*********************
