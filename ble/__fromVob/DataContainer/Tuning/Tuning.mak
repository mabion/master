# $ZFLSHEADER$ ----------------------------------------------------------------
# -----------------------------------------------------------------------------  
#                                                                            
#  Copyright (c) ZF Lenksysteme GmbH, Germany                                
#                All rights reserved                                         
#                                                                            
# ----------------------------------------------------------------------------- 
#                                                                            
#   Dateiname:       Tuning.mak
# 
#   Beschreibung:    Tuning makefile
#                                                                                                    
# -----------------------------------------------------------------------------                                                                           

FEAT = FEAT_DEV_TUNING
$(FEAT)_KON_FILES += $(DAMOS_TEMP_PATH)\lenk.kon
#$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Tuning\l_Honda0.kon
#$(FEAT)_KON_FILES += $(CPR_SRC_BASE)\DataContainer\Tuning\l_Honda1.kon
   
# $END_ZFLSHEADER$ ------------------------------------------------------------ 



# **************************************************************************************************
# HISTORY:
# --------
#  2008-03-05 Boensch, Michael (G51827)
#             \steering_epsdaimlerc218w166\SW\ImplementationSet\Infrastructure\GlobalData\SystemData\SystemData.mak@@\main\1
# --------------------------------------------------------------------------------------------------
#  2008-06-03 Boensch, Michael (G51827)
#             \steering_epsdaimlerc218w166\SW\ImplementationSet\Infrastructure\GlobalData\Tuning\Tuning.mak@@\main\1
# --------------------------------------------------------------------------------------------------
#  2008-06-26 Boensch, Michael (G51827)
#             \steering_epsdaimlerc218w166\SW\ImplementationSet\Infrastructure\GlobalData\Tuning\Tuning.mak@@\main\2
# --------------------------------------------------------------------------------------------------
#  2009-12-09 Berckhemer Uwe SGD EZPA * (g52672)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Tuning\Tuning.mak@@\main\1
# --------------------------------------------------------------------------------------------------
#  2010-04-09 Mergenthaler Thomas SGD EZPA * (g52484)
#             \sgdcc_eps_sdk_dev\EpsDriveSW\ImplementationSet\DataContainer\Tuning\Tuning.mak@@\main\2
# --------------------------------------------------------------------------------------------------
#  2017-07-01 RameshKumar RameenaSri CMT EXT (gx1591)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Tuning\Tuning.mak@@\main\1
# --------------------------------------------------------------------------------------------------
#  2019-04-27 --- (uvh5cob)
#             \sgdcc_eps_honda_sdk\EpsDriveSW\ImplementationSet\DataContainer\Tuning\Tuning.mak@@\main\2
# --------------------------------------------------------------------------------------------------
# **************************************************************************.HE*********************
