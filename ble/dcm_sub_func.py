#############################
# START
#############################
import parameters
import log_sub_func
from io import IOBase
from collections import defaultdict
import os

def dcm_loader(inPath):
    try:
        ptr = open(inPath, 'r', errors='ignore')
        if isinstance(ptr, IOBase):
            log_sub_func.log_msg(parameters.insig_tag + 'Found DCM ' + ptr.name)
        return ptr.read().splitlines()

    except TypeError:
        log_sub_func.log_msg(parameters.insig_tag + 'No DCM found!')
        return 0


def dcm_var_sorter(var_list):
    log_sub_func.log_msg(parameters.insig_tag + 'Reading the DCM file....')
    start = 0
    temp_sorted_list = []
    sorted_list = []
    for line in var_list:

        for var_type in parameters.var_types:
            if var_type in line:
                start = 1

        if start == 1:
            temp_sorted_list.append(line)

            if 'END' in line:
                temp_sorted_list = dcm_clean_up(temp_sorted_list)
                sorted_list.append(temp_sorted_list)
                temp_sorted_list = []
                start = 0

    log_sub_func.log_msg(parameters.insig_tag + 'Found ' + str(len(sorted_list)) + ' variables in the DCM....')

    return sorted_list


def dcm_clean_up(var_list):

    temp_var_list = []
    func = {'FUNKTION': 'NA'}
    for row in var_list:
        rqd = 0

        for field in parameters.var_sig_fields:
            if field in row:
                rqd = 1

        for field in parameters.var_types:
            if field in row:
                rqd = 1

        if 'FUNKTION' in row:
            try:
                func = {'FUNKTION': list(filter(None, row.split(' ')))[1]}
            except IndexError:
                func = {'FUNKTION': 'NA'}

        if rqd == 1:
            temp_var_list.append(row)

    if '__A' not in temp_var_list[0]:
        temp_var_list.append(func)

    return temp_var_list


def dcm_var_coded_sorter(var_list):

    tun_list, other_list = [], []
    tun_dict = defaultdict(list)

    for var in var_list:
       
        if '__A' in var[0]:
            tun_list.append(var) # should be removed
            
            import re
            m = re.search(r'\_+(\d+)\_+A', var[0])
            if m is not None:
                tun_dict[m[1]].append(var)
            del re

        else:
            other_list.append(var)

    log_sub_func.log_msg(parameters.insig_tag + 'Out of these, ' + str(len(tun_list)) + ' are variant coded and ' +
                         str(len(other_list)) + ' are not....')
    return tun_list, other_list, tun_dict


def dcm_pre_loader(inPath):
    dict_file = dict()

    for file in os.listdir(inPath):
        if file.endswith(".dcm", ".DCM", ".Dcm"):
            try:
                ptr = open(os.path.join(inPath, file), 'r')
                log_sub_func.log_msg(parameters.insig_tag + 'Found DCM ' + ptr.name)
                dict_file[file] = ptr.read().splitlines()
            except TypeError:
                log_sub_func.log_msg(parameters.insig_tag + 'No DCM found!')

    if len(dict_file):
        return dict_file
    else:
        return False
