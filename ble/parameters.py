#############################
# START
#############################
import os


var_types = ['FESTWERTEBLOCK',
             'FESTWERT',
             'STUETZSTELLENVERTEILUNG',
             'FESTKENNLINIE',
             'GRUPPENKENNLINIE',
             'KENNLINIE',
             'FESTKENNFELD',
             'GRUPPENKENNFELD',
             'KENNFELD']


var_insig_fields = ['LANGNAME',
                    'EINHEIT_X',
                    'EINHEIT_W',
                    'EINHEIT_Y',
                    'TEXT',
                    'SSTX',
                    'SSTY',
                    'FUNKTION']

var_sig_fields = ['ST/X',
                  'WERT',
                  'ST/Y',
                  'END']

tun_dict = {'_0_': 'D2Ux_normal.kon',
            '_1_': 'D2Ux_sport.kon',
            '_2_': 'D2Ux_track.kon',
            '_3_': 'D2Ux_comfort.kon'}

log_file = open('trace.log', 'w')
log_file.seek(0)
log_file.truncate()
insig_tag = '>>'

tracing = 'On'

common_file_name = 'SystemData_000.kon'
temp_file_name = 'temp.kon'

non_var_coded_mode = [1, 2, 10]
# 0 - Look for function to assign kon-file from 'funct_dict'.
# If function is not available, 'aux_mode' is used for that variable.

# 1 - Look for key strings in the variable name to assign kon-file from key_dict.
# If no strings are available, 'aux_mode' is used for that variable.

# 2 - Check if the variable is already available in any provided file. Update if present.
# If not available in any file, 'aux_mode' is used for that variable.

# 10 - Put all variables in a specific file whose name is defined in 'common_file_name'.
# 11 - Put all variables in a temporary file which can be used for manual processing.

funct_dict = {'EndStop': 'EndStop.kon',
              'AutomaticParkingControlStatemachine': 'SystemData_000.kon',
              'DriveTorqueSteerCompensation' : 'DriverTorqueSteerCompensation.kon',
              'DriveTorqueSteerCompensationPlausi': 'DriverTorqueSteerCompensation.kon',
              'DriveTorqueSteerCompensationCheck': 'DriverTorqueSteerCompensation.kon',
              'ActiveReturn': 'ActiveReturn.kon',
              'AssistanceV2': 'SystemData_000.kon',
              'ActiveReturnInterpol': 'ActiveReturn.kon',
              'ActiveDamping': 'ActiveDamping.kon',
              'SteeringControl': 'SystemData_000.kon',
              'TorqueSensor': 'SystemData_000.kon',
              'PullDriftCompensationMain': 'PullDriftComp.kon',
              'StartStop': 'SystemData_000.kon',
              'HighSpeedDamping': 'SystemData_000.kon',
              'HighSpeedDampingPlausi': 'SystemData_000.kon',
              'ChatterRecognition': 'ChatterRecognition.kon',
              'InertiaCompensation': 'SystemData_000.kon',
              'NA': 'SystemData_000.kon'}

key_dict = {'ActDamp': 'ActiveDamping.kon',
            'ActRet': 'ActiveReturn.kon',
            'ioNTC': 'AnalogSensors.kon',
            'AutParkCtrl': 'AutomaticParkingControlController.kon',
            'BST': 'BasicSteerTorque.kon',
            'cr': 'ChatterRecognition.kon',
            'DTSC': 'DriverTorqueSteerCompensation.kon',
            'EndStop': 'EndStop.kon',
            'JudC': 'JudderCompensation.kon',
            'PDC': 'PullDriftComp.kon'}


def ListFile(inPath, inExtension):
        import glob
        FileList = []
        for filename in glob.iglob(inPath + '**/**', recursive=True):
            if filename.endswith(inExtension):
                FileList.append(filename.replace("\\","/"))
        del glob
        return FileList

def FindFolder(SubDir, Dir):
    return [x[0] for x in os.walk(Dir) if os.path.basename(x)==SubDir]
